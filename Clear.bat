rmdir classes\backup /S /Q
rmdir datamodule\backup /S /Q
rmdir forms\backup /S /Q
rmdir units\backup /S /Q
rmdir backup /S /Q
rmdir lib /S /Q
del *.~*
del classes\*.~*
del units\*.~*
del datamodule\*.~*
del forms\*.~*
del *.bdsproj*
del *.cfg
del *.identcache
del *.dsk
del *.*.local
del *.stat
rmdir __history /S /Q
rmdir classes\__history /S /Q
rmdir datamodule\__history /S /Q
rmdir forms\__history /S /Q
rmdir units\__history /S /Q
rmdir __recovery /S /Q
rmdir classes\__recovery /S /Q
rmdir datamodule\__recovery /S /Q
rmdir forms\__recovery /S /Q
rmdir units\__recovery /S /Q
del *.ddp
del classes\*.ddp
del units\*.ddp
del datamodule\*.ddp
del forms\*.ddp
del *.dcu
del classes\*.dcu
del units\*.dcu
del datamodule\*.dcu
del forms\*.dcu
del *.bak
del classes\*.bak 
del units\*.bak
del datamodule\*.bak
del forms\*.bak
del *.lrs
del classes\*.lrs
del units\*.lrs
del datamodule\*.lrs
del forms\*.lrs
del *.o
del classes\*.o 
del units\*.o
del datamodule\*.o
del forms\*.o
del *.ppu
del clases\*.ppu
del units\*.ppu
del datamodule\*.ppu
del forms\*.ppu
del *.a
del classes\*.a
del units\*.a
del datamodule\*.a
del forms\*.a
del *.compiled
del *.or
pause
