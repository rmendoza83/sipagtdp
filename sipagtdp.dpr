program sipagtdp;

uses
  Vcl.Forms,
  MidasLib,
  ClsGenericoBD in 'classes\ClsGenericoBD.pas',
  ClsUsuario in 'classes\ClsUsuario.pas',
  Utils in 'units\Utils.pas',
  Generico in 'forms\Generico.pas' {FrmGenerico},
  DMBD in 'datamodule\DMBD.pas' {BD: TDataModule},
  DMFR in 'datamodule\DMFR.pas' {DMFastReport: TDataModule},
  ClsDetallePagoBono in 'classes\ClsDetallePagoBono.pas',
  ClsEstado in 'classes\ClsEstado.pas',
  ClsMision in 'classes\ClsMision.pas',
  ClsMunicipio in 'classes\ClsMunicipio.pas',
  ClsPagoBono in 'classes\ClsPagoBono.pas',
  ClsParroquia in 'classes\ClsParroquia.pas',
  ClsBono in 'classes\ClsBono.pas',
  ClsCliente in 'classes\ClsCliente.pas',
  GenericoBuscador in 'forms\GenericoBuscador.pas' {FrmGenericoBuscador},
  Estado in 'forms\Estado.pas' {FrmEstado},
  Mision in 'forms\Mision.pas' {FrmMision},
  Municipio in 'forms\Municipio.pas' {FrmMunicipio},
  Parroquia in 'forms\Parroquia.pas' {FrmParroquia},
  Importador in 'forms\Importador.pas' {FrmImportador},
  Main in 'forms\Main.pas' {FrmMain},
  Login in 'forms\Login.pas' {FrmLogin},
  Bono in 'forms\Bono.pas' {FrmBono},
  Cliente in 'forms\Cliente.pas' {FrmCliente};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
