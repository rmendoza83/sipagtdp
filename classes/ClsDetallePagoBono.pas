unit ClsDetallePagoBono;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils;

type

  {TDetallePagoBono}

  TDetallePagoBono = class(TGenericoBD)
    private
      Id: integer;
      Cedula: string;
      Monto: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetId(varId: integer);
      procedure SetCedula(varCedula: string);
      procedure SetMonto(varMonto: Double);
      function GetId: integer;
      function GetCedula: string;
      function GetMonto: Double;
  end;


implementation

{ TDetallePagoBono }

procedure TDetallePagoBono.Ajustar;
begin

end;

procedure TDetallePagoBono.AntesEliminar;
begin

end;

procedure TDetallePagoBono.AntesInsertar;
begin

end;

procedure TDetallePagoBono.AntesModificar;
begin

end;

constructor TDetallePagoBono.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('DETALLE_PAGO_BONO');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "CEDULA"=$2, "MONTO"=$3 WHERE ("ID"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("ID"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"ID"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TDetallePagoBono.DespuesEliminar;
begin

end;

procedure TDetallePagoBono.DespuesInsertar;
begin

end;

procedure TDetallePagoBono.DespuesModificar;
begin

end;

function TDetallePagoBono.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,1);
  Campos[0] := 'CEDULA';
  Result := Campos;
end;

function TDetallePagoBono.GetCedula: string;
begin
  Result := Cedula;
end;

function TDetallePagoBono.GetCondicion: string;
begin

end;

function TDetallePagoBono.GetId: integer;
begin
  Result := Id;
end;

function TDetallePagoBono.GetMonto: Double;
begin
  Result := Monto;
end;

function TDetallePagoBono.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := Id;
  Params[1] := Cedula;
  Params[2] := Monto;
  Result := Params;
end;

procedure TDetallePagoBono.Mover(Ds: TClientDataSet);
begin
  Id := Ds.FieldValues['ID'];
  Cedula := Ds.FieldValues['CEDULA'];
  Monto := Ds.FieldValues['MONTO'];
end;

procedure TDetallePagoBono.SetCedula(varCedula: string);
begin
  Cedula := varCedula;
end;

procedure TDetallePagoBono.SetId(varId: integer);
begin
  Id := varId;
end;

procedure TDetallePagoBono.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

end.
