unit ClsCliente;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils, DMBD;

type

  {TCliente}

  TCliente = class(TGenericoBD)
    private
      Cedula : string;
      Nacionalidad : string;
      Nombres : string;
      Apellidos : string;
      Sexo : string;
      FechaNacimiento : TDate;
      Direccion : string;
      Telefonos : string;
      Email : string;
      FechaIngreso : TDate;
      CodigoEstado : string;
      CodigoMunicipio : string;
      CodigoParroquia : string;
      CodigoMision : string;
      NumeroCuenta : string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCedula(varCedula: string);
      procedure SetNacionalidad(varNacionalidad: string);
      procedure SetNombres(varNombres: string);
      procedure SetApellidos(varApellidos: string);
      procedure SetSexo(varSexo: string);
      procedure SetFechaNacimiento(varFechaNacimiento: TDate);
      procedure SetDireccion(varDireccion: string);
      procedure SetTelefono(varTelefono: string);
      procedure SetEmail(varEmail: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetCodigoEstado(varCodigoEstado: string);
      procedure SetCodigoMunicipio(varCodigoMunicipio: string);
      procedure SetCodigoParroquia(varCodigoParroquia: string);
      procedure SetCodigoMision(varCodigoMision: string);
      procedure SetNumeroCuenta(varNumeroCuenta: string);
      function GetCedula: string;
      function GetNacionalidad : string;
      function GetNombres: string;
      function GetApellidos: string;
      function GetSexo: string;
      function GetFechaNacimiento: TDate;
      function GetDireccion: string;
      function GetTelefonos: string;
      function GetEmail: string;
      function GetFechaIngreso: TDate;
      function GetCodigoEstado: string;
      function GetCodigoMunicipio: string;
      function GetCodigoParroquia: string;
      function GetCodigoMision: string;
      function GetNumeroCuenta: string;
      function BuscarCodigo: Boolean;
  end;


implementation

{ TCliente }

procedure TCliente.Ajustar;
begin

end;

procedure TCliente.AntesEliminar;
begin

end;

procedure TCliente.AntesInsertar;
begin

end;

procedure TCliente.AntesModificar;
begin

end;

function TCliente.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("CODIGO" = ' + QuotedStr(GetCedula) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

constructor TCliente.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('CLIENTE');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(20)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "NACIONALIDAD"=$2, "NOMBRES"=$3, "APELLIDOS"=$4, "SEXO"=$5, "FECHA_NACIMIENTO"=$6, "DIRECCION"=$7, "TELEFONOS"=$8, "EMAIL"=$9, "FECHA_INGRESO"=$10, ' +
                   '"CODIGO_ESTADO"=$11, "CODIGO_MUNICIPIO"=$12, "CODIGO_PARROQUIA"=$13, "CODIGO_MISION"=$14, "NUMERO_CUENTA"=$15 WHERE ("CEDULA"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CEDULA"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"CEDULA"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TCliente.DespuesEliminar;
begin

end;

procedure TCliente.DespuesInsertar;
begin

end;

procedure TCliente.DespuesModificar;
begin

end;

function TCliente.GetApellidos: string;
begin
  Result := Apellidos;
end;

function TCliente.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,13);
  Campos[0] := 'CEDULA';
  Campos[1] := 'NACIONALIDAD';
  Campos[2] := 'NOMBRES';
  Campos[3] := 'APELLIDOS';
  Campos[4] := 'SEXO';
  Campos[5] := 'DIRECCION';
  Campos[6] := 'TELEFONOS';
  Campos[7] := 'EMAIL';
  Campos[8] := 'CODIGO_ESTADO';
  Campos[9] := 'CODIGO_MUNICIPIO';
  Campos[10] := 'CODIGO_PARROQUIA';
  Campos[11] := 'CODIGO_MISION';
  Campos[12] := 'NUMERO_CUENTA';
  Result := Campos;
end;

function TCliente.GetCedula: string;
begin
  Result := Cedula;
end;

function TCliente.GetCodigoEstado: string;
begin
  Result := CodigoEstado;
end;

function TCliente.GetCodigoMision: string;
begin
  Result := CodigoMision
end;

function TCliente.GetCodigoMunicipio: string;
begin
  Result := CodigoMunicipio;
end;

function TCliente.GetCodigoParroquia: string;
begin
  Result := CodigoParroquia;
end;

function TCliente.GetCondicion: string;
begin
  Result := '(CEDULA = ' + QuotedStr(Cedula) + ')';
end;

function TCliente.GetDireccion: string;
begin
  Result := Direccion;
end;

function TCliente.GetEmail: string;
begin
  Result := Email;
end;

function TCliente.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TCliente.GetFechaNacimiento: TDate;
begin
  Result := FechaNacimiento;
end;

function TCliente.GetNacionalidad: string;
begin
  Result := Nacionalidad;
end;

function TCliente.GetNombres: string;
begin
  Result := Nombres;
end;

function TCliente.GetNumeroCuenta: string;
begin
  Result := NumeroCuenta;
end;

function TCliente.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,15);
  Params[0] := Cedula;
  Params[1] := Nacionalidad;
  Params[2] := Nombres;
  Params[3] := Apellidos;
  Params[4] := Sexo;
  Params[5] := FechaNacimiento;
  Params[6] := Direccion;
  Params[7] := Telefonos;
  Params[8] := Email;
  Params[9] := FechaIngreso;
  Params[10] := CodigoEstado;
  Params[11] := CodigoMunicipio;
  Params[12] := CodigoParroquia;
  Params[13] := CodigoMision;
  Params[14] := NumeroCuenta;
  Result := Params;
end;

function TCliente.GetSexo: string;
begin
  Result := Sexo;
end;

function TCliente.GetTelefonos: string;
begin
  Result := Telefonos;
end;

procedure TCliente.Mover(Ds: TClientDataSet);
begin
  Cedula := Ds.FieldValues['CEDULA'];
  Nacionalidad := Ds.FieldValues['NACIONALIDAD'];
  Nombres := Ds.FieldValues['NOMBRES'];
  Apellidos := Ds.FieldValues['APELLIDOS'];
  Sexo := Ds.FieldValues['SEXO'];
  FechaNacimiento := Ds.FieldByName('FECHA_NACIMIENTO').AsDateTime;
  Direccion := Ds.FieldValues['DIRECCION'];
  Telefonos := Ds.FieldValues['TELEFONOS'];
  Email := Ds.FieldValues['EMAIL'];
  FechaIngreso := Ds.FieldByName('FECHA_INGRESO').AsDateTime;
  CodigoEstado := Ds.FieldValues['CODIGO_ESTADO'];
  CodigoMunicipio := Ds.FieldValues['CODIGO_MUNICIPIO'];
  CodigoParroquia := Ds.FieldValues['CODIGO_PARROQUIA'];
  CodigoMision := Ds.FieldValues['CODIGO_MISION'];
  NumeroCuenta := Ds.FieldValues['NUMERO_CUENTA'];
end;

procedure TCliente.SetApellidos(varApellidos: string);
begin
  Apellidos := varApellidos;
end;

procedure TCliente.SetCedula(varCedula: string);
begin
  Cedula := varCedula;
end;

procedure TCliente.SetCodigoEstado(varCodigoEstado: string);
begin
  CodigoEstado := varCodigoEstado;
end;

procedure TCliente.SetCodigoMision(varCodigoMision: string);
begin
  CodigoMision := varCodigoMision;
end;

procedure TCliente.SetCodigoMunicipio(varCodigoMunicipio: string);
begin
  CodigoMunicipio := varCodigoMunicipio;
end;

procedure TCliente.SetCodigoParroquia(varCodigoParroquia: string);
begin
  CodigoParroquia := varCodigoParroquia;
end;

procedure TCliente.SetDireccion(varDireccion: string);
begin
  Direccion := varDireccion;
end;

procedure TCliente.SetEmail(varEmail: string);
begin
  Email := varEmail;
end;

procedure TCliente.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TCliente.SetFechaNacimiento(varFechaNacimiento: TDate);
begin
  FechaNacimiento := varFechaNacimiento;
end;

procedure TCliente.SetNacionalidad(varNacionalidad: string);
begin
  Nacionalidad := varNacionalidad;
end;

procedure TCliente.SetNombres(varNombres: string);
begin
  Nombres := varNombres
end;

procedure TCliente.SetNumeroCuenta(varNumeroCuenta: string);
begin
  NumeroCuenta := varNumeroCuenta;
end;

procedure TCliente.SetSexo(varSexo: string);
begin
  Sexo := varSexo;
end;

procedure TCliente.SetTelefono(varTelefono: string);
begin
  Telefonos := varTelefono;
end;

end.
