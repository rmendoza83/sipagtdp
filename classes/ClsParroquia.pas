unit ClsParroquia;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils, DMBD;

type

  {TParroquia}

  TParroquia = class(TGenericoBD)
    private
      CodigoEstado: string;
      CodigoMunicipio: string;
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoEstado(varCodigoEstado: string);
      procedure SetCodigoMunicipio(varCodigoMunicipio: string);
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigoEstado: string;
      function GetCodigoMunicipio: string;
      function GetCodigo: string;
      function GetDescripcion: string;
      function BuscarCodigo: Boolean;
  end;

implementation

{ TParroquia }

procedure TParroquia.Ajustar;
begin

end;

procedure TParroquia.AntesEliminar;
begin

end;

procedure TParroquia.AntesInsertar;
begin

end;

procedure TParroquia.AntesModificar;
begin

end;

function TParroquia.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("CODIGO" = ' + QuotedStr(GetCodigo) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

constructor TParroquia.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('PARROQUIA');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "DESCRIPCION"=$4 WHERE ("CODIGO_ESTADO"=$1) AND ("CODIGO_MUNICIPIO"=$2) AND ("CODIGO"=$3)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CODIGO_ESTADO"=$1) AND ("CODIGO_MUNICIPIO"=$2) AND ("CODIGO"=$3)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"CODIGO_ESTADO"';
  ClavesPrimarias[1] := '"CODIGO_MUNICIPIO"';
  ClavesPrimarias[2] := '"CODIGO"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TParroquia.DespuesEliminar;
begin

end;

procedure TParroquia.DespuesInsertar;
begin

end;

procedure TParroquia.DespuesModificar;
begin

end;

function TParroquia.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,4);
  Campos[0] := 'CODIGO_ESTADO';
  Campos[1] := 'CODIGO_MUNICIPIO';
  Campos[3] := 'CODIGO';
  Campos[2] := 'DESCRIPCION';
  Result := Campos;
end;

function TParroquia.GetCodigo: string;
begin
  Result := Codigo;
end;

function TParroquia.GetCodigoEstado: string;
begin
  Result := CodigoEstado;
end;

function TParroquia.GetCodigoMunicipio: string;
begin
  Result := CodigoMunicipio;
end;

function TParroquia.GetCondicion: string;
begin
  Result := '("CODIGO_ESTADO" = ' + QuotedStr(CodigoEstado) + ') AND ' +
            '("CODIGO_MUNICIPIO" = ' + QuotedStr(CodigoMunicipio) + ') AND ' +
            '("CODIGO" = ' + QuotedStr(Codigo) + ')';
end;

function TParroquia.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TParroquia.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := CodigoEstado;
  Params[1] := CodigoMunicipio;
  Params[2] := Codigo;
  Params[3] := Descripcion;
  Result := Params;
end;

procedure TParroquia.Mover(Ds: TClientDataSet);
begin
  CodigoEstado := Ds.FieldValues['CODIGO_ESTADO'];
  CodigoMunicipio := Ds.FieldValues['CODIGO_MUNICIPIO'];
  Codigo := Ds.FieldValues['CODIGO'];
  Descripcion := Ds.FieldValues['DESCRIPCION'];
end;

procedure TParroquia.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TParroquia.SetCodigoEstado(varCodigoEstado: string);
begin
  CodigoEstado := varCodigoEstado;
end;

procedure TParroquia.SetCodigoMunicipio(varCodigoMunicipio: string);
begin
  CodigoMunicipio := varCodigoMunicipio;
end;

procedure TParroquia.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

end.
