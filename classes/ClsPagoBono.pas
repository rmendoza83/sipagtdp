unit ClsPagoBono;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils, Variants;

type

  {TPagoBono}

  TPagoBono = class(TGenericoBD)
    private
      Id: Integer;
      CodigoBono: string;
      MesPago: Integer;
      CodigoMision: string;
      FechaIngreso: TDate;
      FechaPago: TDate;
      Cantidad: Integer;
      Total: Double;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetId(varId: Integer);
      procedure SetCodigoBono(varCodigoBono: string);
      procedure SetMesPago(varMesPago: Integer);
      procedure SetCodigoMision(varCodigoMision: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetFechaPago(varFechaPago: TDate);
      procedure SetCantidad(varCantidad: Integer);
      procedure SetTotal(varTotal: Double);
      function GetId: Integer;
      function GetCodigoBono: string;
      function GetMesPago: Integer;
      function GetCodigoMision: string;
      function GetFechaIngreso: TDate;
      function GetFechaPago: TDate;
      function GetCantidad: Integer;
      function GetTotal: Double;
  end;

implementation

{ TPagoBono }

procedure TPagoBono.Ajustar;
begin

end;

procedure TPagoBono.AntesEliminar;
begin

end;

procedure TPagoBono.AntesInsertar;
begin

end;

procedure TPagoBono.AntesModificar;
begin

end;

constructor TPagoBono.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('PAGO_BONO');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',',CreateDollarsParams(8)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "ID"=$1, "FECHA_INGRESO"=$5, "FECHA_PAGO"=$6, "CANTIDAD"=$7, "TOTAL"=$8 ' +
                  'WHERE ("CODIGO_BONO"=$2) AND ("MES_PAGO"=$3) AND ("CODIGO_MISION"=$4)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CODIGO_BONO"=$2) AND ("MES_PAGO"=$3) AND ("CODIGO_MISION"=$4)');
  SetLength(ClavesPrimarias,3);
  ClavesPrimarias[0] := '"CODIGO_BONO"';
  ClavesPrimarias[1] := '"MES_PAGO"';
  ClavesPrimarias[2] := '"CODIGO_MISION"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TPagoBono.DespuesEliminar;
begin

end;

procedure TPagoBono.DespuesInsertar;
begin

end;

procedure TPagoBono.DespuesModificar;
begin

end;

function TPagoBono.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'CODIGO_BONO';
  Campos[1] := 'CODIGO_MISION';
  Result := Campos;
end;

function TPagoBono.GetCantidad: Integer;
begin
  Result := Cantidad;
end;

function TPagoBono.GetCodigoBono: string;
begin
  Result := CodigoBono;
end;

function TPagoBono.GetCodigoMision: string;
begin
  Result := CodigoMision;
end;

function TPagoBono.GetCondicion: string;
begin
  Result := '("CODIGO_BONO" = ' + QuotedStr(CodigoBono) + ') AND ' +
            '("MES_PAGO" = ' + IntToStr(MesPago) + ') AND ' +
            '("CODIGO_MISION" = ' + QuotedStr(CodigoMision) + ')';
end;

function TPagoBono.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TPagoBono.GetFechaPago: TDate;
begin
  Result := FechaPago;
end;

function TPagoBono.GetId: Integer;
begin
  Result := Id;
end;

function TPagoBono.GetMesPago: Integer;
begin
  Result := MesPago;
end;

function TPagoBono.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,8);
  Params[0] := Id;
  Params[1] := CodigoBono;
  Params[2] := MesPago;
  Params[3] := CodigoMision;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := VarFromDateTime(FechaPago);
  Params[6] := Cantidad;
  Params[7] := Total;
  Result := Params;
end;

function TPagoBono.GetTotal: Double;
begin
  Result := Total;
end;

procedure TPagoBono.Mover(Ds: TClientDataSet);
begin
  Id := Ds.FieldValues['ID'];
  CodigoBono := Ds.FieldValues['CODIGO_BONO'];
  MesPago := Ds.FieldValues['MES_PAGO'];
  CodigoMision := Ds.FieldValues['CODIGO_MISION'];
  FechaIngreso := Ds.FieldValues['FECHA_INGRESO'];
  FechaPago := Ds.FieldValues['FECHA_PAGO'];
  Cantidad := Ds.FieldValues['CANTIDAD'];
  Total := Ds.FieldValues['TOTAL'];
end;

procedure TPagoBono.SetCantidad(varCantidad: Integer);
begin
  Cantidad := varCantidad;
end;

procedure TPagoBono.SetCodigoBono(varCodigoBono: string);
begin
  CodigoBono := varCodigoBono;
end;

procedure TPagoBono.SetCodigoMision(varCodigoMision: string);
begin
  CodigoMision := varCodigoMision;
end;

procedure TPagoBono.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TPagoBono.SetFechaPago(varFechaPago: TDate);
begin
  FechaPago := varFechaPago;
end;

procedure TPagoBono.SetId(varId: Integer);
begin
  Id := varId;
end;

procedure TPagoBono.SetMesPago(varMesPago: Integer);
begin
  MesPago := varMesPago;
end;

procedure TPagoBono.SetTotal(varTotal: Double);
begin
  Total := varTotal;
end;

end.

