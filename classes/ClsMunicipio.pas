unit ClsMunicipio;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils, DMBD;

type
  {TMunicipio}

  TMunicipio = class(TGenericoBD)
    private
      CodigoEstado: string;
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigoEstado(varCodigoEstado: string);
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigoEstado: string;
      function GetCodigo: string;
      function GetDescripcion: string;
      function BuscarCodigo: Boolean;
  end;

implementation

{ TMunicipio }

procedure TMunicipio.Ajustar;
begin

end;

procedure TMunicipio.AntesEliminar;
begin

end;

procedure TMunicipio.AntesInsertar;
begin

end;

procedure TMunicipio.AntesModificar;
begin

end;

constructor TMunicipio.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('MUNICIPIO');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(3)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "DESCRIPCION"=$3 WHERE ("CODIGO_ESTADO"=$1) AND ("CODIGO"=$2) ');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CODIGO_ESTADO"=$1) AND ("CODIGO"=$2)');
  SetLength(ClavesPrimarias,2);
  ClavesPrimarias[0] := '"CODIGO_ESTADO"';
  ClavesPrimarias[1] := '"CODIGO"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TMunicipio.DespuesEliminar;
begin

end;

procedure TMunicipio.DespuesInsertar;
begin

end;

procedure TMunicipio.DespuesModificar;
begin

end;

function TMunicipio.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'CODIGO_ESTADO';
  Campos[1] := 'CODIGO';
  Campos[2] := 'DESCRIPCION';
  Result := Campos;
end;

function TMunicipio.GetCodigo: string;
begin
  Result := Codigo;
end;

function TMunicipio.GetCodigoEstado: string;
begin
  Result := CodigoEstado;
end;

function TMunicipio.GetCondicion: string;
begin
  Result := '"CODIGO" = ' + QuotedStr(Codigo);
end;

function TMunicipio.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TMunicipio.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,3);
  Params[0] := CodigoEstado;
  Params[1] := Codigo;
  Params[2] := Descripcion;
  Result := Params;
end;

procedure TMunicipio.Mover(Ds: TClientDataSet);
begin
  CodigoEstado := Ds.FieldValues['CODIGO_ESTADO'];
  Codigo := Ds.FieldValues['CODIGO'];
  Descripcion := Ds.FieldValues['DESCRIPCION'];
end;

procedure TMunicipio.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TMunicipio.SetCodigoEstado(varCodigoEstado: string);
begin
  CodigoEstado := varCodigoEstado;
end;

procedure TMunicipio.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

function TMunicipio.BuscarCodigo: Boolean;
var
  Ds: TClientDataSet;
begin
  Result := False;
  Ds := BD.EjecutarQueryCliDs('SELECT * FROM ' + GetTabla + ' WHERE ("CODIGO" = ' + QuotedStr(GetCodigo) + ')');
  if (Ds.RecordCount > 0) then
  begin
    Result := True;
    Mover(Ds);
  end;
end;

end.
