unit ClsMision;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils;

type

  {TMision}

  TMision = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TMision }

procedure TMision.Ajustar;
begin

end;

procedure TMision.AntesEliminar;
begin

end;

procedure TMision.AntesInsertar;
begin

end;

procedure TMision.AntesModificar;
begin

end;

constructor TMision.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('MISION');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "DESCRIPCION"=$2 WHERE ("CODIGO"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CODIGO"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"CODIGO"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TMision.DespuesEliminar;
begin

end;

procedure TMision.DespuesInsertar;
begin

end;

procedure TMision.DespuesModificar;
begin

end;

function TMision.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'CODIGO';
  Campos[1] := 'DESCRIPCION';
  Result := Campos;
end;

function TMision.GetCodigo: string;
begin
  Result := Codigo;
end;

function TMision.GetCondicion: string;
begin
  Result := '"CODIGO" = ' + QuotedStr(Codigo);
end;

function TMision.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TMision.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

procedure TMision.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['CODIGO'];
  Descripcion := Ds.FieldValues['DESCRIPCION'];
end;

procedure TMision.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TMision.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

end.
