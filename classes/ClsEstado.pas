unit ClsEstado;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils;

type

  {TEstado}

  TEstado = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      function GetCodigo: string;
      function GetDescripcion: string;
  end;

implementation

{ TEstado }

procedure TEstado.Ajustar;
begin

end;

procedure TEstado.AntesEliminar;
begin

end;

procedure TEstado.AntesInsertar;
begin

end;

procedure TEstado.AntesModificar;
begin

end;

constructor TEstado.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('ESTADO');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(2)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "DESCRIPCION"=$2 WHERE ("CODIGO"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CODIGO"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"CODIGO"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TEstado.DespuesEliminar;
begin

end;

procedure TEstado.DespuesInsertar;
begin

end;

procedure TEstado.DespuesModificar;
begin

end;

function TEstado.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'CODIGO';
  Campos[1] := 'DESCRIPCION';
  Result := Campos;
end;

function TEstado.GetCodigo: string;
begin
  Result := Codigo;
end;

function TEstado.GetCondicion: string;
begin
  Result := '"CODIGO" = ' + QuotedStr(Codigo);
end;

function TEstado.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TEstado.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,2);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Result := Params;
end;

procedure TEstado.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['CODIGO'];
  Descripcion := Ds.FieldValues['DESCRIPCION'];
end;

procedure TEstado.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TEstado.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

end.
