unit ClsBono;

interface

uses
  Classes, SysUtils, DBClient, ClsGenericoBD, Utils, Variants;

type

  {TBono}

  TBono = class(TGenericoBD)
    private
      Codigo: string;
      Descripcion: string;
      Monto: Double;
      FechaIngreso: TDate;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: string; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetCodigo(varCodigo: string);
      procedure SetDescripcion(varDescripcion: string);
      procedure SetMonto(varMonto: Double);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      function GetCodigo: string;
      function GetDescripcion: string;
      function GetMonto: Double;
      function GetFechaIngreso: TDate;
  end;


implementation

{ TBono }

procedure TBono.Ajustar;
begin

end;

procedure TBono.AntesEliminar;
begin

end;

procedure TBono.AntesInsertar;
begin

end;

procedure TBono.AntesModificar;
begin

end;

constructor TBono.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('BONO');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(4)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "DESCRIPCION"=$2, "MONTO"=$3, "FECHA_INGRESO"=$4 WHERE ("CODIGO"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("CODIGO"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"CODIGO"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TBono.DespuesEliminar;
begin

end;

procedure TBono.DespuesInsertar;
begin

end;

procedure TBono.DespuesModificar;
begin

end;

function TBono.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,2);
  Campos[0] := 'CODIGO';
  Campos[1] := 'DESCRIPCION';
  Result := Campos;
end;

function TBono.GetCodigo: string;
begin
  Result := Codigo;
end;

function TBono.GetCondicion: string;
begin
  Result := '(CODIGO = ' + QuotedStr(Codigo) + ')';
end;

function TBono.GetDescripcion: string;
begin
  Result := Descripcion;
end;

function TBono.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TBono.GetMonto: Double;
begin
  Result := Monto;
end;

function TBono.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,4);
  Params[0] := Codigo;
  Params[1] := Descripcion;
  Params[2] := Monto;
  Params[3] := VarFromDateTime(FechaIngreso);
  Result := Params;
end;

procedure TBono.Mover(Ds: TClientDataSet);
begin
  Codigo := Ds.FieldValues['CODIGO'];
  Descripcion := Ds.FieldValues['DESCRIPCION'];
  Monto := Ds.FieldValues['MONTO'];
  FechaIngreso := Ds.FieldValues['FECHA_INGRESO'];
end;

procedure TBono.SetCodigo(varCodigo: string);
begin
  Codigo := varCodigo;
end;

procedure TBono.SetDescripcion(varDescripcion: string);
begin
  Descripcion := varDescripcion;
end;

procedure TBono.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TBono.SetMonto(varMonto: Double);
begin
  Monto := varMonto;
end;

end.
