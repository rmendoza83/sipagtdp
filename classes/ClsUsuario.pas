unit ClsUsuario;

interface

uses
  Classes, SysUtils, DBClient, DMBD, ClsGenericoBD, Utils, Variants;

type

  { TUsuario }

  TUsuario = class(TGenericoBD)
    private
      Username: string;
      Nombres: string;
      Apellidos: string;
      Userpass: string;
      FechaIngreso: TDate;
      SuperUser: Boolean;
    protected
      procedure Ajustar; override;
      procedure Mover(Ds: TClientDataSet); override;
      function GetParametros: TArrayVariant; override;
      function GetCampos: TArrayString; override;
      function GetCondicion: String; override;
      procedure AntesInsertar; override;
      procedure AntesModificar; override;
      procedure AntesEliminar; override;
      procedure DespuesInsertar; override;
      procedure DespuesModificar; override;
      procedure DespuesEliminar; override;
    public
      constructor Create;
      procedure SetUsername(varUsername: string);
      procedure SetNombres(varNombres: string);
      procedure SetApellidos(varApellidos: string);
      procedure SetUserpass(varUserpass: string);
      procedure SetFechaIngreso(varFechaIngreso: TDate);
      procedure SetSuperUser(varSuperUser: Boolean);
      function GetUsername: string;
      function GetNombres: string;
      function GetApellidos: string;
      function GetUserpass: string;
      function GetFechaIngreso: TDate;
      function GetSuperUser: Boolean;
  end;

implementation

{ TUsuario }

procedure TUsuario.Ajustar;
begin

end;

procedure TUsuario.Mover(Ds: TClientDataSet);
begin
  Username := Ds.FieldValues['USERNAME'];
  Nombres := Ds.FieldValues['NOMBRES'];
  Apellidos := Ds.FieldValues['APELLIDOS'];
  Userpass := Ds.FieldValues['USERPASS'];
  FechaIngreso := Ds.FieldValues['FECHA_INGRESO'];
  SuperUser := Ds.FieldValues['SUPER_USER'];
end;

function TUsuario.GetParametros: TArrayVariant;
var
  Params: TArrayVariant;
begin
  SetLength(Params,6);
  Params[0] := Username;
  Params[1] := Nombres;
  Params[2] := Apellidos;
  Params[3] := Userpass;
  Params[4] := VarFromDateTime(FechaIngreso);
  Params[5] := SuperUser;
  Result := Params;
end;

function TUsuario.GetCampos: TArrayString;
var
  Campos: TArrayString;
begin
  SetLength(Campos,3);
  Campos[0] := 'USERNAME';
  Campos[1] := 'NOMBRES';
  Campos[2] := 'APELLIDOS';
  Result := Campos;
end;

function TUsuario.GetCondicion: String;
begin
  Result := '"USERNAME" = ' + QuotedStr(Username);
end;

procedure TUsuario.AntesInsertar;
begin
  SetUserpass(GetUsername);
end;

procedure TUsuario.AntesModificar;
begin

end;

procedure TUsuario.AntesEliminar;
begin

end;

procedure TUsuario.DespuesInsertar;
begin

end;

procedure TUsuario.DespuesModificar;
begin

end;

procedure TUsuario.DespuesEliminar;
begin

end;

constructor TUsuario.Create;
var
  ClavesPrimarias: TArrayString;
begin
  inherited Create;
  SetTabla('USUARIO');
  SetStrSQLInsert('INSERT INTO ' + GetTabla + ' VALUES (' + Implode(',', CreateDollarsParams(6)) + ')');
  SetStrSQLUpdate('UPDATE ' + GetTabla + ' SET "NOMBRES"=$2, "APELLIDOS"=$3, "USERPASS"=$4, "FECHA_INGRESO"=$5, "SUPER_USER"=$6 WHERE ("USERNAME"=$1)');
  SetStrSQLDelete('DELETE FROM ' + GetTabla + ' WHERE ("USERNAME"=$1)');
  SetLength(ClavesPrimarias,1);
  ClavesPrimarias[0] := '"USERNAME"';
  SetArrayClavesPrimarias(ClavesPrimarias);
end;

procedure TUsuario.SetUsername(varUsername: string);
begin
  Username := varUsername;
end;

procedure TUsuario.SetNombres(varNombres: string);
begin
  Nombres := varNombres;
end;

procedure TUsuario.SetApellidos(varApellidos: string);
begin
  Apellidos := varApellidos;
end;

procedure TUsuario.SetUserpass(varUserpass: string);
begin
  Userpass := GetMD5(varUserpass);
end;

procedure TUsuario.SetFechaIngreso(varFechaIngreso: TDate);
begin
  FechaIngreso := varFechaIngreso;
end;

procedure TUsuario.SetSuperUser(varSuperUser: Boolean);
begin
  SuperUser := varSuperUser;
end;

function TUsuario.GetUsername: string;
begin
  Result := Username;
end;

function TUsuario.GetNombres: string;
begin
  Result := Nombres;
end;

function TUsuario.GetApellidos: string;
begin
  Result := Apellidos;
end;

function TUsuario.GetUserpass: string;
begin
  Result := Userpass;
end;

function TUsuario.GetFechaIngreso: TDate;
begin
  Result := FechaIngreso;
end;

function TUsuario.GetSuperUser: Boolean;
begin
  Result := SuperUser;
end;

end.

