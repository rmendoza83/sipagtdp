object DMFastReport: TDMFastReport
  OldCreateOrder = False
  Height = 225
  Width = 284
  object frxReport: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42962.966615960600000000
    ReportOptions.LastChange = 43031.995846088000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 32
    Top = 16
    Datasets = <
      item
        DataSet = frxReport.ZQryInformeVenda
        DataSetName = 'informe_venda'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
      object ZBD: TfrxZEOSDatabase
        Port = 0
        Protocol = 'sqlite-3'
        DatabaseName = 'xslvp.db'
        LoginPrompt = False
        Connected = True
        pLeft = 40
        pTop = 12
      end
      object ZQryInformeVenda: TfrxZEOSQuery
        UserName = 'informe_venda'
        CloseDataSource = True
        BCDToCurrency = False
        IgnoreDupParams = False
        Params = <>
        SQL.Strings = (
          'SELECT *'
          
            'FROM "main"."informe_venda"                                     ' +
            '                  ')
        Database = frxReport.ZBD
        pLeft = 40
        pTop = 68
        Parameters = <>
      end
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 355.600000000000000000
      PaperHeight = 215.900000000000000000
      PaperSize = 5
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 18.897650000000000000
        Width = 1268.410268000000000000
        object Memo3: TfrxMemoView
          Left = 7.559060000000000000
          Width = 381.732530000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Lista de Vendas')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 1268.410268000000000000
        DataSet = frxReport.ZQryInformeVenda
        DataSetName = 'informe_venda'
        RowCount = 0
        object informe_vendavenda_id: TfrxMemoView
          Left = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'venda_id'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."venda_id"]')
          ParentFont = False
        end
        object informe_vendavenda: TfrxMemoView
          Left = 84.826840000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'venda'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."venda"]')
          ParentFont = False
        end
        object informe_vendanumero_cupom: TfrxMemoView
          Left = 162.519790000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'numero_cupom'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."numero_cupom"]')
          ParentFont = False
        end
        object informe_vendadata_viagem: TfrxMemoView
          Left = 241.889920000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DataField = 'data_viagem'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Memo.UTF8W = (
            '[informe_venda."data_viagem"]')
        end
        object informe_vendahora_viagem: TfrxMemoView
          Left = 328.819110000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          DataField = 'hora_viagem'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Memo.UTF8W = (
            '[informe_venda."hora_viagem"]')
        end
        object informe_vendatrecho_viagem: TfrxMemoView
          Left = 419.527830000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DataField = 'trecho_viagem'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Memo.UTF8W = (
            '[informe_venda."trecho_viagem"]')
        end
        object informe_vendapassagem1: TfrxMemoView
          Left = 536.693260000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."passagem"] + '#39' - '#39' + [informe_venda."descri"]')
          ParentFont = False
        end
        object informe_vendapreco: TfrxMemoView
          Left = 805.039890000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'preco'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."preco"]')
          ParentFont = False
        end
        object informe_vendaplaca: TfrxMemoView
          Left = 884.410020000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'placa'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."placa"]')
          ParentFont = False
        end
        object informe_vendanome: TfrxMemoView
          Left = 963.780150000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DataField = 'nome'
          DataSet = frxReport.ZQryInformeVenda
          DataSetName = 'informe_venda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[informe_venda."nome"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 75.590600000000000000
        Width = 1268.410268000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Venda ID')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 84.826840000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Venda')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 162.519790000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nro Cupom')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 241.889920000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data Viagem')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 328.819110000000000000
          Width = 86.929133860000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Hora Viagem')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 419.527830000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Trecho Viagem')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 536.693260000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Passagem')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 805.039890000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Preco')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 884.410020000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Placa')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 963.780150000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
      end
    end
  end
end
