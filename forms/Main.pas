unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.XPMan, ClsUsuario;

type
  TFrmMain = class(TForm)
    TemaXP: TXPManifest;
    MenuPrincipal: TMainMenu;
    Principal1: TMenuItem;
    CerrarSesion1: TMenuItem;
    N1: TMenuItem;
    Salir1: TMenuItem;
    Operaciones1: TMenuItem;
    Maestro1: TMenuItem;
    Estados1: TMenuItem;
    Municipios1: TMenuItem;
    Parroquias1: TMenuItem;
    Misiones1: TMenuItem;
    Bonos1: TMenuItem;
    Sistema1: TMenuItem;
    Usuarios1: TMenuItem;
    Ayuda1: TMenuItem;
    Ayuda2: TMenuItem;
    N2: TMenuItem;
    Acercade1: TMenuItem;
    Clientes1: TMenuItem;
    N3: TMenuItem;
    Pagos1: TMenuItem;
    ImportarDatos1: TMenuItem;
    N4: TMenuItem;
    procedure Salir1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Estados1Click(Sender: TObject);
    procedure Municipios1Click(Sender: TObject);
    procedure Parroquias1Click(Sender: TObject);
    procedure Misiones1Click(Sender: TObject);
    procedure Bonos1Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure ImportarDatos1Click(Sender: TObject);
  private
    { Private declarations }
    Usuario: TUsuario;
    function Login: Boolean;
    procedure FreeForm(var Form);
    procedure IniForm(Clase: TComponentClass; var Form);
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

uses
  DMBD,
  Login,
  Cliente,
  Estado,
  Municipio,
  Parroquia,
  Mision,
  Bono,
  Importador;

{$R *.dfm}

procedure TFrmMain.Bonos1Click(Sender: TObject);
begin
  IniForm(TFrmBono,FrmBono);
end;

procedure TFrmMain.Clientes1Click(Sender: TObject);
begin
  IniForm(TFrmCliente,FrmCliente);
end;

procedure TFrmMain.Estados1Click(Sender: TObject);
begin
  IniForm(TFrmEstado,FrmEstado);
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Application.CreateForm(TBD,BD);
  Usuario := TUsuario.Create;
  Screen.Cursor := crDefault;
  if (not Application.Terminated) then
  begin
    if (not Login) then
    begin
      Application.Terminate;
    end
    else
    begin
      Usuario.Buscar;
      Top := 0;
      Left := 0;
    end;
  end;
end;

procedure TFrmMain.FreeForm(var Form);
begin
  if ((TForm(Form) <> nil) and (not TForm(Form).Visible)) then
  begin
    FreeAndNil(Form);
  end;
end;

procedure TFrmMain.ImportarDatos1Click(Sender: TObject);
begin
  IniForm(TFrmImportador,FrmImportador);
end;

procedure TFrmMain.IniForm(Clase: TComponentClass; var Form);
var
  FormMod: TComponent;
begin
  try
    FreeForm(Form);
    if (TForm(Form) = nil) then
      begin
        FormMod := TComponent(Clase.NewInstance);
        TComponent(Form) := FormMod;
        FormMod.Create(Self);
      end;
    TForm(Form).Show;
  except
  end;
end;

function TFrmMain.Login: Boolean;
var
  xModalResult: TModalResult;
begin
  Result := True;
  //Lanzado Ventana de Inicio de Sesion
  FrmLogin := TFrmLogin.Create(Self);
  xModalResult := FrmLogin.ShowModal;
  Usuario.SetUsername(FrmLogin.LoginUsuario);
  Usuario.Buscar;
  FreeAndNil(FrmLogin);
  if (xModalResult = mrCancel) then
  begin
    Result := False;
  end;
end;

procedure TFrmMain.Misiones1Click(Sender: TObject);
begin
  IniForm(TFrmMision,FrmMision);
end;

procedure TFrmMain.Municipios1Click(Sender: TObject);
begin
  IniForm(TFrmMunicipio,FrmMunicipio);
end;

procedure TFrmMain.Parroquias1Click(Sender: TObject);
begin
  IniForm(TFrmParroquia,FrmParroquia);
end;

procedure TFrmMain.Salir1Click(Sender: TObject);
begin
  Close;
end;

end.
