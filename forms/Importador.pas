unit Importador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Datasnap.DBClient,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.FlexCel.Core, FlexCel.XlsAdapter,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;

type
  TPersona = class
    public
      Cedula: string;
      Nombre: string;
      Estado: string;
      Municipio: string;
      Parroquia: string;
  end;
  TThreadRequest = class(TThread)
    private
      SG: TStringGrid;
      PB: TProgressBar;
      FilaInicial: Integer;
      FilaFinal: Integer;
      HTTPRequest: TIdHTTP;
      Persona: TPersona;
      FilaActual: Integer;
      CheckedCNE: Boolean;
      NRequests: Integer;
      function CheckCNE(Nacionalidad: Char; Cedula: string): Boolean;
      procedure UpdateSG;
    public
      constructor Create(ASG: TStringGrid; APB: TProgressBar; AFilaInicial: Integer; AFilaFinal: Integer); overload;
      procedure Execute; override;
  end;
  TArrayThreadRequest = array of TThreadRequest;

  TFrmImportador = class(TForm)
    PanelEncabezado: TPanel;
    PanelBotones: TPanel;
    PageMaster: TPageControl;
    PB: TProgressBar;
    BtnCerrar: TBitBtn;
    BtnGuardar: TBitBtn;
    BtnAbrir: TSpeedButton;
    LblArchivo: TLabel;
    TxtArchivo: TEdit;
    ODlg: TOpenDialog;
    BtnEjecutar: TBitBtn;
    TabDataCompleta: TTabSheet;
    SGDC: TStringGrid;
    BtnRevisar: TBitBtn;
    TabDataIncompleta: TTabSheet;
    SGDI: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnAbrirClick(Sender: TObject);
    procedure BtnEjecutarClick(Sender: TObject);
    procedure BtnRevisarClick(Sender: TObject);
    procedure SGDCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtnCerrarClick(Sender: TObject);
  private
    { Private declarations }
    XlsFile: TXlsFile;
    ArrayThreadRequest: TArrayThreadRequest;
    procedure ConfiguraStringGrid(SG: TStringGrid; IncluyeCNE: Boolean);
  public
    { Public declarations }
  end;

const
  MAX_THREADS = 20;
  MAX_REQUESTS = 5;

var
  FrmImportador: TFrmImportador;

implementation

uses
  GraphUtil;

{$R *.dfm}

procedure TFrmImportador.BtnAbrirClick(Sender: TObject);
begin
  if (ODlg.Execute) then
  begin
    TxtArchivo.Text := ODlg.FileName;
  end;
end;

procedure TFrmImportador.BtnCerrarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmImportador.BtnEjecutarClick(Sender: TObject);
var
  i, j: Integer;
begin
  if (Length(Trim(TxtArchivo.Text)) > 0) then
  begin
    Screen.Cursor := crHourGlass;
    XlsFile.Open(TxtArchivo.Text);
    try
      //Configurando PB
      PB.Min := 0;
      PB.Max := XlsFile.GetRowCount(1);
      PB.Step := 1;
      //Configurar StringGrid
      XlsFile.ActiveSheet := 1;
      SGDC.RowCount := XlsFile.GetRowCount(1) + 1;
      for i := 2 to XlsFile.GetRowCount(1) do
      begin
        for j := 1 to 5 do
        begin
          SGDC.Cells[j - 1,i - 1] := XlsFile.GetCellValue(i,j).ToString;
        end;
        PB.StepIt;
        if ((i mod 25) = 0) then
        begin
          Application.ProcessMessages;
        end;
      end;
    except
      ShowMessage('Fucking Error...');
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFrmImportador.BtnRevisarClick(Sender: TObject);
var
  i, j: Integer;
begin
  SetLength(ArrayThreadRequest,MAX_THREADS);
  j := (SGDC.RowCount - 1) Div MAX_THREADS;
  for i := Low(ArrayThreadRequest) to High(ArrayThreadRequest) do
  begin
    ArrayThreadRequest[i] := TThreadRequest.Create(SGDC,PB,(i * j) + 1,(i * j) + j);
    ArrayThreadRequest[i].Start;
  end;
end;

procedure TFrmImportador.ConfiguraStringGrid(SG: TStringGrid; IncluyeCNE: Boolean);
begin
  with SG do
  begin
    ColCount := 5;
    Cells[0,0] := 'XLS Nacionalidad';
    Cells[1,0] := 'XLS Cedula';
    Cells[2,0] := 'XLS Nombres';
    Cells[3,0] := 'XLS Mision';
    Cells[4,0] := 'XLS Estado';
    ColWidths[0] := 100;
    ColWidths[1] := 75;
    ColWidths[2] := 300;
    ColWidths[3] := 75;
    ColWidths[4] := 75;
    if (IncluyeCNE) then
    begin
      ColCount := 11;
      Cells[5,0] := 'Validado CNE';
      Cells[6,0] := 'CNE Cedula';
      Cells[7,0] := 'CNE Nombres';
      Cells[8,0] := 'CNE Estado';
      Cells[9,0] := 'CNE Municipio';
      Cells[10,0] := 'CNE Parroquia';
      ColWidths[5] := 100;
      ColWidths[6] := 75;
      ColWidths[7] := 300;
      ColWidths[8] := 75;
      ColWidths[9] := 100;
      ColWidths[10] := 100;
    end;
    RowCount := 1;
  end;
end;

procedure TFrmImportador.FormCreate(Sender: TObject);
begin
  XlsFile := TXlsFile.Create;
  //Configurando StringGrids
  ConfiguraStringGrid(SGDC,True);
  ConfiguraStringGrid(SGDI,False);
end;

procedure TFrmImportador.FormDestroy(Sender: TObject);
begin
  if (Assigned(XlsFile)) then
  begin
    XlsFile.Destroy;
  end;
end;

procedure TFrmImportador.SGDCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  SG: TStringGrid;
begin
  SG := (Sender as TStringGrid);
  with SG.Canvas do
  begin
    if (ARow = 0) then
    begin
      Font.Style := [fsBold];
    end
    else
    begin
      Font.Style := [];
    end;
    if (gdSelected in State) then //Celda Seleccionada
    begin
      Brush.Color := clBtnFace;
      Font.Color := clWindowText;
    end
    else
    begin
      if (gdFixed in State) then //Celda Titulo
      begin
        Brush.Color := clBtnFace;
        Font.Color := clWindowText;
      end
      else //Celda Normal
      begin
        Brush.Color := clWindow;
        Font.Color := clWindowText;
      end;
    end;
    if (ARow = 0)  then
    begin
      Rect.Left := Rect.Left - 2;
      Rect.TopLeft.X := Rect.TopLeft.X - 2;
      GradientFillCanvas(SG.Canvas,SG.GradientStartColor,SG.GradientEndColor,Rect,gdVertical);
    end
    else
    begin
      FillRect(Rect);
    end;
    Brush.Style := bsClear;
    TextOut(Rect.Left + 2, Rect.Top + 2,SG.Cells[ACol,ARow]);
  end;
end;

{ TThreadRequest }

function TThreadRequest.CheckCNE(Nacionalidad: Char; Cedula: string): Boolean;
const
  URI = 'http://www.cne.gob.ve/web/registro_electoral/ce.php?';
  StrHTMLTable = '<table cellpadding="2" width="530">';
var
  sw: Boolean;
  HTMLResult: string;
  AuxString: string;

  function GetValueTD(S: string): string;
  var
    SEndTD: string;
  begin
    SEndTD := Trim(Copy(S,Pos('<td align="left">',S) + Length('<td align="left">'),Length(S)));
    Result := Trim(Copy(AuxString,Pos('<td align="left">',AuxString) + Length('<td align="left">'),Pos('</td>',SEndTD) - 1));
    Result := StringReplace(Result,'<b>','',[rfIgnoreCase , rfReplaceAll]);
    Result := StringReplace(Result,'</b>','',[rfIgnoreCase , rfReplaceAll]);
  end;

  function IsValidResponse(Response: string): Boolean;
  begin
    Result := (Pos('ESTADO:',UpperCase(Response)) > 0) and 
              (Pos('MUNICIPIO:',UpperCase(Response)) > 0) and
              (Pos('PARROQUIA:',UpperCase(Response)) > 0);
  end;
begin
  sw := False;
  try
    if (NRequests = 0) then
    begin
      if (HTTPRequest.Connected) then
      begin
        HTTPRequest.Disconnect;
      end;
      NRequests := MAX_REQUESTS;
    end;
    HTMLResult := HTTPRequest.Get(URI + 'nacionalidad=' + Nacionalidad + '&cedula=' + Cedula);
    if (IsValidResponse(HTMLResult)) then
    begin
      HTMLResult := Trim(Copy(HTMLResult,Pos(StrHTMLTable,HTMLResult) + Length(StrHTMLTable),Length(HTMLResult)));
      HTMLResult := Trim(Copy(HTMLResult,1,Pos('</table>',HTMLResult) - 1));
      with Persona do
      begin
        AuxString := Trim(Copy(HTMLResult,Pos('C�dula:',HTMLResult),Pos('Nombre:',HTMLResult)));
        Cedula := GetValueTD(AuxString);
        AuxString := Trim(Copy(HTMLResult,Pos('Nombre:',HTMLResult),Pos('Estado:',HTMLResult)));
        Nombre := GetValueTD(AuxString);
        AuxString := Trim(Copy(HTMLResult,Pos('Estado:',HTMLResult),Pos('Municipio:',HTMLResult)));
        Estado := GetValueTD(AuxString);
        AuxString := Trim(Copy(HTMLResult,Pos('Municipio:',HTMLResult),Pos('Parroquia:',HTMLResult)));
        Municipio := GetValueTD(AuxString);
        AuxString := Trim(Copy(HTMLResult,Pos('Parroquia:',HTMLResult),Pos('Centro:',HTMLResult)));
        Parroquia := GetValueTD(AuxString);
      end;
      sw := True;
    end
    else
    begin
      sw := False;
    end;
  except
    sw := False;
  end;
  NRequests := NRequests - 1;
  Result := sw;
end;

constructor TThreadRequest.Create(ASG: TStringGrid; APB: TProgressBar;
  AFilaInicial, AFilaFinal: Integer);
begin
  inherited Create(True);
  PB := APB;
  SG := ASG;
  FilaInicial := AFilaInicial;
  FilaFinal := AFilaFinal;
  Persona := TPersona.Create;
  HTTPRequest := TIdHTTP.Create;
  NRequests := MAX_REQUESTS;
  with HTTPRequest do
  begin
    Request.CacheControl := 'no-cache';
    Request.Connection := 'keep-alive';
    Request.ContentType := 'text/html';
    ProtocolVersion := pv1_1; 
  end;
  Priority := tpHighest;
  FreeOnTerminate := True;
end;

procedure TThreadRequest.Execute;
var
  i, reintentos: Integer;
  Nacionalidad: Char;
  Cedula: string;
begin
  for i := FilaInicial to FilaFinal do
  begin
    Nacionalidad := SG.Cells[0,i][1];
    Cedula := SG.Cells[1,i];
    FilaActual := i;
    reintentos := 3;
    repeat
      CheckedCNE := CheckCNE(Nacionalidad,Cedula);
      if (CheckedCNE) then
      begin
        Break;
      end
      else
      begin
        reintentos := reintentos - 1;
        Sleep(Random(50) + 50);
      end;
    until (reintentos = 0);
    Synchronize(UpdateSG);
    if ((i mod 50) = 0) then
    begin
      Application.ProcessMessages;
    end;
  end;
  Application.ProcessMessages;
  Terminate;
end;

procedure TThreadRequest.UpdateSG;
begin
  PB.StepIt;
  if (CheckedCNE) then
  begin
    with SG do
    begin
      Cells[5,FilaActual] := 'S';
      Cells[6,FilaActual] := Persona.Cedula;
      Cells[7,FilaActual] := Persona.Nombre;
      Cells[8,FilaActual] := Persona.Estado;
      Cells[9,FilaActual] := Persona.Municipio;
      Cells[10,FilaActual] := Persona.Parroquia;
    end;
  end
  else
  begin
    SG.Cells[5,FilaActual] := 'N';
  end;
  SG.Refresh;
end;

end.
