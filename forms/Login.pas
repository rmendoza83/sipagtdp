unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPMan, StdCtrls, Buttons, Utils, ClsUsuario;

const
  //Constantes de Control de Error en el Inicio de Sesion
  NoExiste = 'N';
  ClaveInvalida = 'C';
  CuentaExpirada = 'E';

type

  { TFrmLogin }
  
  TFrmLogin = class(TForm)
    LblTitulo: TLabel;
    LblLogin: TLabel;
    LblPassword: TLabel;
    BtnCancelar: TBitBtn;
    BtnAceptar: TBitBtn;
    TemaXP: TXPManifest;
    TxtLogin: TEdit;
    TxtPassword: TEdit;
    procedure TxtLoginKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    Intentos: Integer;
    Cerrar: Boolean;
    Usuario: TUsuario;
    function ValidarUsuario(var CharError: Char): Boolean;
    procedure ProcesarInicio;
  public
    { Public declarations }
    LoginUsuario: String;
  end;

var
  FrmLogin: TFrmLogin;

implementation

{ TFrmLogin }

{$R *.dfm}

procedure TFrmLogin.TxtLoginKeyPress(Sender: TObject; var Key: Char);
begin
  if (Pos(Key,TeclasValidas) = 0) then
  begin
    Key := #0;
  end;
end;

procedure TFrmLogin.FormCreate(Sender: TObject);
begin
  Cerrar := False;
  Intentos := 3;
  Usuario := TUsuario.Create;
end;

procedure TFrmLogin.FormShow(Sender: TObject);
begin
  TxtLogin.SetFocus; 
end;

procedure TFrmLogin.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := Cerrar;
end;

procedure TFrmLogin.BtnCancelarClick(Sender: TObject);
begin
  if (MessageDlg('�Esta seguro que desea Cancelar el Inicio de Sesi�n?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    Cerrar := True;
    Close;
  end;
end;

procedure TFrmLogin.BtnAceptarClick(Sender: TObject);
begin
  if (Length(Trim(TxtLogin.Text)) = 0) then
  begin
    MessageDlg('Debe Indicar el Login del Usuario!!!', mtError, [mbOK], 0);
    TxtLogin.SetFocus;
  end
  else
  begin
    if (Length(Trim(TxtPassword.Text)) = 0) then
    begin
      MessageDlg('Debe Indicar la Contrase�a del Usuario!!!', mtError, [mbOK], 0);
      TxtPassword.SetFocus;
    end
    else
    begin
      ProcesarInicio;
    end;
  end;
end;

procedure TFrmLogin.ProcesarInicio;
var
  CharError: Char;
  StrError: String;
begin
  CharError := #0;
  if (ValidarUsuario(CharError)) then
  begin
    //Mensaje de Bienvenida
    MessageDlg('Bienvenido, ' + Usuario.GetNombres + ' ' + Usuario.GetApellidos +#13+#10+''+#13+#10+'Presione OK para continuar la Carga del Sistema.', mtInformation, [mbOK], 0);
    //Asignando Datos de Inicio de Sesion
    Cerrar := True;
    ModalResult := mrOk;
    LoginUsuario := Usuario.GetUsername;
  end
  else
  begin
    Intentos := Intentos - 1;
    if (Intentos > 0) then
    begin
      case CharError of
        NoExiste: StrError := 'El Login indicado NO Existe en la Base de Datos.';
        ClaveInvalida: StrError := 'Clave o Contrase�a Inv�lida';
        CuentaExpirada : StrError := 'La Cuenta ha Expirado!!! Consulte con el Administrador del Sistema.';
      end;
      StrError := StrError + #13 + #10 + #13 + #10 + 'Le Quedan ' + IntToStr(Intentos) + ' Intentos...';
      MessageDlg(StrError, mtError, [mbOK], 0);
      TxtLogin.Text := '';
      TxtPassword.Text := '';
      TxtLogin.SetFocus;
    end
    else
    begin
      MessageDlg('Por su Seguridad el Sistema ser� Cerrado. Recuerde sus datos y vuelva a intentarlo m�s tarde.', mtInformation, [mbOK], 0);
      ModalResult := mrCancel;
      Cerrar := True;
      Close;
    end;
  end;
end;

function TFrmLogin.ValidarUsuario(var CharError: Char): Boolean;
begin
  Usuario.SetUsername(TxtLogin.Text);
  if (Usuario.Buscar) then
  begin
    if (Usuario.GetUserpass = GetMD5(TxtPassword.Text)) then
    begin
      CharError := #0;
      Result := True;
      //Falta Verificar Cuenta Expirada
    end
    else
    begin
      CharError := ClaveInvalida;
      Result := False;
    end;
  end
  else
  begin
    CharError := NoExiste;
    Result := False;
  end;
end;

end.
