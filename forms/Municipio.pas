unit Municipio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Generico, Data.DB, Vcl.XPMan,
  System.ImageList, Vcl.ImgList, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ToolWin, Vcl.DBCtrls, RxLookup, ClsEstado, ClsMunicipio,
  Utils, DBClient;

type
  TFrmMunicipio = class(TFrmGenerico)
    LblDescripcion: TLabel;
    TxtDescripcion: TEdit;
    LblCodigoEstado: TLabel;
    LblCodigo: TLabel;
    TxtCodigo: TEdit;
    CmbEstado: TRxDBLookupCombo;
    BtnBuscarEstado: TBitBtn;
    TxtEstado: TDBText;
    DSEstado: TDataSource;
    procedure BtnBuscarEstadoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
  private
    Clase: TMunicipio;
    Data: TClientDataSet;
    Estado: TEstado;
    DataEstado: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmMunicipio: TFrmMunicipio;

implementation

uses
  GenericoBuscador;

{$R *.dfm}

{ TFrmMunicipio }

procedure TFrmMunicipio.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmMunicipio.Asignar;
begin
  with Clase do
  begin
    CmbEstado.KeyValue := GetCodigoEstado;
    DataEstado.Locate('codigo',CmbEstado.KeyValue,[loCaseInsensitive]);
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
  end;
end;

procedure TFrmMunicipio.BtnBuscarEstadoClick(Sender: TObject);
begin
  Buscador(Self,Estado,'codigo','Busqueda de Estado',nil,DataEstado);
  CmbEstado.KeyValue := DataEstado.FieldValues['codigo'];
end;

procedure TFrmMunicipio.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmMunicipio.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmMunicipio.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.BuscarCodigo;
  Asignar;
  TxtCodigo.Enabled := False;
  CmbEstado.SetFocus;
end;

procedure TFrmMunicipio.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Municipio Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmMunicipio.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TMunicipio.Create;
  //Inicializando DataSets Secundarios
  Estado:= TEstado.Create;
  DataEstado:= Estado.ObtenerLista;
  DataEstado.First;
  DSEstado.DataSet:= DataEstado;
  TxtEstado.DataSource:= DSEstado;
  TxtEstado.DataField:= 'descripcion';
  ConfigurarRxDBLookupCombo(CmbEstado,DSEstado,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmMunicipio.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmMunicipio.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Municipio Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Municipio Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmMunicipio.Imprimir;
begin

end;

procedure TFrmMunicipio.Limpiar;
begin
  CmbEstado.KeyValue := '';
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
end;

procedure TFrmMunicipio.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigoEstado(DSTemp.FieldValues['codigo_estado']);
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmMunicipio.Obtener;
begin
  with Clase do
  begin
    SetCodigoEstado(CmbEstado.Text);
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
  end;
end;

procedure TFrmMunicipio.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataEstado:= Estado.ObtenerLista;
  DataEstado.First;
  DSEstado.DataSet:= DataEstado;
end;

function TFrmMunicipio.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Concepto *)
    //Validando Codigo Estado
  if (Length(Trim(CmbEstado.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar El Estado!!!');
  end;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
