object FrmMain: TFrmMain
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 
    'Sipagtdp - Sistema de Pagos para los Tarjetahabientes de la Patr' +
    'ia'
  ClientHeight = 82
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MenuPrincipal
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object TemaXP: TXPManifest
    Left = 32
    Top = 52
  end
  object MenuPrincipal: TMainMenu
    Left = 28
    Top = 4
    object Principal1: TMenuItem
      Caption = 'Principal'
      object CerrarSesion1: TMenuItem
        Caption = 'Cerrar Sesion'
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Salir1: TMenuItem
        Caption = 'Salir'
        OnClick = Salir1Click
      end
    end
    object Operaciones1: TMenuItem
      Caption = 'Operaciones'
      object Pagos1: TMenuItem
        Caption = 'Pagos'
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object ImportarDatos1: TMenuItem
        Caption = 'Importar Datos'
        OnClick = ImportarDatos1Click
      end
    end
    object Maestro1: TMenuItem
      Caption = 'Maestro'
      object Clientes1: TMenuItem
        Caption = 'Clientes'
        OnClick = Clientes1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Estados1: TMenuItem
        Caption = 'Estados'
        OnClick = Estados1Click
      end
      object Municipios1: TMenuItem
        Caption = 'Municipios'
        OnClick = Municipios1Click
      end
      object Parroquias1: TMenuItem
        Caption = 'Parroquias'
        OnClick = Parroquias1Click
      end
      object Misiones1: TMenuItem
        Caption = 'Misiones'
        OnClick = Misiones1Click
      end
      object Bonos1: TMenuItem
        Caption = 'Bonos'
        OnClick = Bonos1Click
      end
    end
    object Sistema1: TMenuItem
      Caption = 'Sistema'
      object Usuarios1: TMenuItem
        Caption = 'Usuarios'
      end
    end
    object Ayuda1: TMenuItem
      Caption = 'Ayuda'
      object Ayuda2: TMenuItem
        Caption = 'Ayuda'
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Acercade1: TMenuItem
        Caption = 'Acerca de...'
      end
    end
  end
end
