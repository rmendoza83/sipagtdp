unit Parroquia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Generico, Data.DB, Vcl.XPMan,
  System.ImageList, Vcl.ImgList, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ToolWin, Vcl.DBCtrls, RxLookup, ClsParroquia, ClsEstado,
  ClsMunicipio, Utils, DBClient;

type
  TFrmParroquia = class(TFrmGenerico)
    LblCodigoEstado: TLabel;
    LblCodigo: TLabel;
    LblDescripcion: TLabel;
    CmbEstado: TRxDBLookupCombo;
    BtnBuscarEstado: TBitBtn;
    TxtEstado: TDBText;
    TxtCodigo: TEdit;
    TxtDescripcion: TEdit;
    LblCodigoMunicipio: TLabel;
    CmbMunicipio: TRxDBLookupCombo;
    BtnBuscarMunicipio: TBitBtn;
    TxtMunicipio: TDBText;
    DSEstado: TDataSource;
    DSMunicipio: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure BtnBuscarEstadoClick(Sender: TObject);
    procedure BtnBuscarMunicipioClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TParroquia;
    Data: TClientDataSet;
    Estado: TEstado;
    DataEstado: TClientDataSet;
    Municipio: TMunicipio;
    DataMunicipio: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmParroquia: TFrmParroquia;

implementation

uses
  GenericoBuscador;
{$R *.dfm}

{ TFrmParroquia }

procedure TFrmParroquia.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCodigo.SetFocus;
end;

procedure TFrmParroquia.Asignar;
begin
  with Clase do
  begin
    CmbEstado.KeyValue := GetCodigoEstado;
    DataEstado.Locate('codigo',CmbEstado.KeyValue,[loCaseInsensitive]);
    CmbMunicipio.KeyValue := GetCodigoMunicipio;
    DataMunicipio.Locate('codigo',CmbMunicipio.KeyValue,[loCaseInsensitive]);
    TxtCodigo.Text := GetCodigo;
    TxtDescripcion.Text := GetDescripcion;
  end;
end;

procedure TFrmParroquia.BtnBuscarEstadoClick(Sender: TObject);
begin
  Buscador(Self,Estado,'codigo','Busqueda de Estado',nil,DataEstado);
  CmbEstado.KeyValue := DataEstado.FieldValues['codigo'];
end;

procedure TFrmParroquia.BtnBuscarMunicipioClick(Sender: TObject);
begin
  Buscador(Self,Municipio,'codigo','Busqueda de Municipio',nil,DataMunicipio);
  CmbMunicipio.KeyValue := DataMunicipio.FieldValues['codigo'];
end;

procedure TFrmParroquia.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmParroquia.Cancelar;
begin
  TxtCodigo.Enabled := True;
end;

procedure TFrmParroquia.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.BuscarCodigo;
  Asignar;
  TxtCodigo.Enabled := False;
  CmbEstado.SetFocus;
end;

procedure TFrmParroquia.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCodigo(Data.FieldValues['codigo']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'La Parroquia Ha Sido Eliminada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmParroquia.FormCreate(Sender: TObject);
begin
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TParroquia.Create;
  //Inicializando DataSets Secundarios
  Estado:= TEstado.Create;
  Municipio:= TMunicipio.Create;
  DataEstado:= Estado.ObtenerLista;
  DataMunicipio:= Municipio.ObtenerLista;
  DataEstado.First;
  DataMunicipio.First;
  DSEstado.DataSet:= DataEstado;
  DSMunicipio.DataSet:= DataMunicipio;
  TxtEstado.DataSource:= DSEstado;
  TxtMunicipio.DataSource:= DSMunicipio;
  TxtEstado.DataField:= 'descripcion';
  TxtMunicipio.DataField:= 'descripcion';
  ConfigurarRxDBLookupCombo(CmbEstado,DSEstado,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbMunicipio,DSMunicipio,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmParroquia.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmParroquia.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCodigo(TxtCodigo.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'El C�digo Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCodigo.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'La Parroquia Ha Sido Agregada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'La Parroquia Ha Sido Editada Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCodigo.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmParroquia.Imprimir;
begin

end;

procedure TFrmParroquia.Limpiar;
begin
  CmbEstado.KeyValue := '';
  CmbMunicipio.KeyValue := '';
  TxtCodigo.Text := '';
  TxtDescripcion.Text := '';
end;

procedure TFrmParroquia.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCodigoEstado(DSTemp.FieldValues['codigo_estado']);
      SetCodigoMunicipio(DSTemp.FieldValues['codigo_municipio']);
      SetCodigo(DSTemp.FieldValues['codigo']);
      SetDescripcion(DSTemp.FieldValues['descripcion']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;

procedure TFrmParroquia.Obtener;
begin
  with Clase do
  begin
    SetCodigoEstado(CmbEstado.Text);
    SetCodigoMunicipio(CmbMunicipio.Text);
    SetCodigo(TxtCodigo.Text);
    SetDescripcion(TxtDescripcion.Text);
  end;
end;

procedure TFrmParroquia.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataEstado:= Estado.ObtenerLista;
  DataEstado.First;
  DSEstado.DataSet:= DataEstado;
  DataMunicipio:= Municipio.ObtenerLista;
  DataMunicipio.First;
  DSMunicipio.DataSet:= DataMunicipio;
end;

function TFrmParroquia.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Concepto *)
    //Validando Codigo Estado
  if (Length(Trim(CmbEstado.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar El Estado!!!');
  end;
    //Validando Codigo Municipio
  if (Length(Trim(CmbMunicipio.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar El Municipio!!!');
  end;
  //Validando Codigo
  if (Length(Trim(TxtCodigo.Text)) = 0) then
  begin
    ListaErrores.Add('El C�digo No Puede Estar En Blanco!!!');
  end;
  //Validando Descripcion
  if (Length(Trim(TxtDescripcion.Text)) = 0) then
  begin
    ListaErrores.Add('La Descripci�n No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;

end.
