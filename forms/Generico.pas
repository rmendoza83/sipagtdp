unit Generico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, DB, XPMan, ComCtrls, ToolWin, StdCtrls, ExtCtrls,
  Buttons, Grids, DBGrids, Utils, System.ImageList;

type
  TFrmGenerico = class(TForm)
    DS: TDataSource;
    Imagenes: TImageList;
    BH: TToolBar;
    TBtnAgregar: TToolButton;
    TBtnEditar: TToolButton;
    TBtnEliminar: TToolButton;
    TBtnS1: TToolButton;
    TBtnGuardar: TToolButton;
    TBtnCancelar: TToolButton;
    TBtnS2: TToolButton;
    TBtnRefrescar: TToolButton;
    TBtnS3: TToolButton;
    TBtnImprimir: TToolButton;
    TemaXP: TXPManifest;
    Pag: TPageControl;
    TabBusqueda: TTabSheet;
    TabDatos: TTabSheet;
    PanelBusqueda: TPanel;
    LblCriterio: TLabel;
    TxtCriterio: TEdit;
    BtnBuscar: TBitBtn;
    GrdBusqueda: TDBGrid;
    procedure GrdBusquedaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure GrdBusquedaDblClick(Sender: TObject);
    procedure TxtCriterioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PagChanging(Sender: TObject; var AllowChange: Boolean);
    procedure TBtnAgregarClick(Sender: TObject);
    procedure TBtnCancelarClick(Sender: TObject);
    procedure TBtnEditarClick(Sender: TObject);
    procedure TBtnEliminarClick(Sender: TObject);
    procedure TBtnGuardarClick(Sender: TObject);
    procedure TBtnImprimirClick(Sender: TObject);
    procedure TBtnRefrescarClick(Sender: TObject);
    procedure DSStateChange(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitarEdicion(Value: Boolean);
  protected
    { Protected declarations }
    ProcAgregar: TProcObject;
    ProcEditar: TProcObject;
    ProcEliminar: TProcObject;
    ProcGuardar: TProcObject;
    ProcCancelar: TProcObject;
    ProcRefrescar: TProcObject;
    ProcImprimir: TProcObject;
    ProcBuscar: TProcObject;
    Agregando: Boolean;
    Editando: Boolean;
    Eliminando: Boolean;
    function Validar: Boolean; virtual; abstract;
  public
    { Public declarations }
  end;

var
  FrmGenerico: TFrmGenerico;

implementation

{$R *.dfm}

procedure TFrmGenerico.BtnBuscarClick(Sender: TObject);
begin
  if (Assigned(ProcBuscar)) then
  begin
    ProcBuscar;
  end;
end;

procedure TFrmGenerico.DSStateChange(Sender: TObject);
var
  i: Integer;
begin
  if (Assigned(Ds.DataSet)) then
  begin
    if (DS.DataSet.State = dsBrowse) then
    begin
      with GrdBusqueda do
      begin
        for i := 0 to Columns.Count - 1 do
        begin
          //Columns[i].Title.Caption := NombreCampoBDToNombreCapital(Columns[i].Title.Caption);
        end;
      end;
    end;
  end;
end;

procedure TFrmGenerico.FormCreate(Sender: TObject);
begin
  ProcAgregar := nil;
  ProcEditar := nil;
  ProcEliminar := nil;
  ProcGuardar := nil;
  ProcCancelar := nil;
  ProcRefrescar := nil;
  ProcImprimir := nil;
  ProcBuscar := nil;
  Pag.ActivePage := TabBusqueda;
  GrdBusqueda.DataSource := DS;
  //Parametro := TParametro.Create;
  //Asignando Decimales por Defecto
  //Parametro.SetNombreParametro('numero_decimales');
  {if (not Parametro.Buscar) then
  begin
    Parametro.SetCategoria('GENERAL');
    Parametro.SetTipoDato('INTEGER');
    Parametro.SetValor('2');
    Parametro.Insertar;
  end;
  BuscarDisplayFormat(Self,Parametro.ObtenerValorAsInteger);}
end;

procedure TFrmGenerico.GrdBusquedaCellClick(Column: TColumn);
begin
  if (Column.Field.DataType = ftBoolean) then
  begin
    {toggle True and False}
    Column.Grid.DataSource.DataSet.Edit;
    Column.Field.Value := not Column.Field.AsBoolean;
    {immediate post - see for yourself whether you want this}
    Column.Grid.DataSource.DataSet.Post;
    {you may add additional functionality here, to be processed after the change was made}
  end;
end;

procedure TFrmGenerico.GrdBusquedaDblClick(Sender: TObject);
begin
  Pag.ActivePage := TabDatos;
end;

procedure TFrmGenerico.GrdBusquedaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  CtrlState: array[Boolean] of Integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
begin
  if (Column.Field.DataType = ftBoolean) then
  begin
    if ((gdSelected in State) or (gdFocused in State)) then
    begin
      if (dgRowSelect in GrdBusqueda.Options) then
      begin
        GrdBusqueda.Canvas.Brush.Color := clHighlight;
      end
      else
      begin
        GrdBusqueda.Canvas.Brush.Color := GrdBusqueda.FixedColor;
      end;
    end
    else
    begin
      GrdBusqueda.Canvas.Brush.Color := GrdBusqueda.FixedColor;
    end;
    GrdBusqueda.Canvas.FillRect(Rect);
    if (VarIsNull(Column.Field.Value)) then
    begin
      DrawFrameControl(GrdBusqueda.Canvas.Handle,Rect, DFC_BUTTON, DFCS_BUTTONCHECK or DFCS_INACTIVE); {grayed}
    end
    else
    begin
      DrawFrameControl(GrdBusqueda.Canvas.Handle,Rect, DFC_BUTTON, CtrlState[Column.Field.AsBoolean]) ; {checked or unchecked}
    end;
  end;
end;

procedure TFrmGenerico.HabilitarEdicion(Value: Boolean);
begin
  TabDatos.Enabled := Value;
  TBtnAgregar.Enabled := not Value;
  TBtnEditar.Enabled := not Value;
  TBtnEliminar.Enabled := not Value;
  TBtnRefrescar.Enabled := not Value;
  TBtnImprimir.Enabled := not Value;
  TBtnGuardar.Enabled := Value;
  TBtnCancelar.Enabled := Value;
  TabBusqueda.Enabled := not Value;
end;

procedure TFrmGenerico.PagChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if ((not Agregando) and (not Editando) and (not Eliminando)) then
  begin
    AllowChange := True;
  end
  else
  begin
    AllowChange := False;
  end;
end;

procedure TFrmGenerico.TBtnAgregarClick(Sender: TObject);
begin
  if (Assigned(ProcAgregar)) then
  begin
    Pag.ActivePage := TabDatos;
    Agregando := True;
    Editando := False;
    Eliminando := False;
    HabilitarEdicion(True);
    ProcAgregar;
  end;
end;

procedure TFrmGenerico.TBtnCancelarClick(Sender: TObject);
begin
  if (MessageBox(Self.Handle, '�Esta Seguro Que Desea Cancelar?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
  begin
    if (Assigned(ProcCancelar)) then
    begin
      ProcCancelar;
      Agregando := False;
      Editando := False;
      Eliminando := False;
      HabilitarEdicion(False);
      Pag.ActivePage := TabBusqueda;
    end;
  end;
end;

procedure TFrmGenerico.TBtnEditarClick(Sender: TObject);
begin
  if (Assigned(ProcEditar)) then
  begin
    if (Assigned(DS.DataSet)) then
    begin
      if (DS.DataSet.RecordCount > 0) then
      begin
        Pag.ActivePage := TabDatos;
        Agregando := False;
        Editando := True;
        Eliminando := False;
        HabilitarEdicion(True);
        ProcEditar;
      end
      else
      begin
        MessageBox(Self.Handle, Cadena('No Hay Registros Para El Proceso de Edici�n!!!'), PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      end;
    end;
  end;
end;

procedure TFrmGenerico.TBtnEliminarClick(Sender: TObject);
begin
  if (Assigned(DS.DataSet)) then
  begin
    if (DS.DataSet.RecordCount > 0) then
    begin
      if (MessageBox(Self.Handle, '�Esta Seguro Que Desea Eliminar?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
      begin
        if (Assigned(ProcEliminar)) then
        begin
          Agregando := False;
          Editando := False;
          Eliminando := True;
          ProcEliminar;
          Eliminando := False;
          Pag.ActivePage := TabBusqueda;
        end;
      end;
    end
    else
    begin
      MessageBox(Self.Handle, Cadena('No Hay Registros Para El Proceso de Eliminaci�n!!!'), PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end;
end;

procedure TFrmGenerico.TBtnGuardarClick(Sender: TObject);
begin
  if (Validar) then
  begin
    if (MessageBox(Self.Handle, '�Esta Seguro Que Los Datos Estan Correctos?', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_YESNO or MB_TOPMOST or MB_TASKMODAL) = mrYes) then
    begin
      if (Assigned(ProcGuardar)) then
      begin
        ProcGuardar;
        Agregando := False;
        Editando := False;
        Eliminando := False;
        HabilitarEdicion(False);
        Pag.ActivePage := TabBusqueda;
      end;
    end;
  end;
end;

procedure TFrmGenerico.TBtnImprimirClick(Sender: TObject);
begin
  if (Assigned(ProcImprimir)) then
  begin
    ProcImprimir;
  end;
end;

procedure TFrmGenerico.TBtnRefrescarClick(Sender: TObject);
begin
  if ((not Agregando) and (not Editando) and (not Eliminando)) then
  begin
    if (Assigned(ProcRefrescar)) then
    begin
      ProcRefrescar;
    end;
  end;
end;

procedure TFrmGenerico.TxtCriterioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
      begin
        if (Assigned(ProcBuscar)) then
        begin
          ProcBuscar;
        end;
      end;
  end;
end;

end.
