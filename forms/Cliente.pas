unit Cliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Generico, Data.DB, Vcl.XPMan,
  System.ImageList, Vcl.ImgList, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ToolWin, Vcl.DBCtrls, RxLookup, ClsCliente,
  Vcl.Mask, RxToolEdit, ClsParroquia, ClsEstado, ClsMunicipio, ClsMision, Utils, DBClient;

type
  TFrmCliente = class(TFrmGenerico)
    LblRif: TLabel;
    CmbPrefijoRif: TComboBox;
    TxtCedula: TEdit;
    LblNombre: TLabel;
    TxtRazonSocial: TEdit;
    LblSexo: TLabel;
    CmbSexo: TComboBox;
    LblFechaNacimiento: TLabel;
    DatFechaNacimiento: TDateEdit;
    LblDireccionFiscal: TLabel;
    TxtDireccionFiscal1: TEdit;
    LblTelefonos: TLabel;
    TxtTelefonos: TEdit;
    lblEmail: TLabel;
    TxtEmail: TEdit;
    LblFechaIngreso: TLabel;
    DatFechaIngreso: TDateEdit;
    LblCodigoEstado: TLabel;
    CmbEstado: TRxDBLookupCombo;
    BtnBuscarEstado: TBitBtn;
    TxtEstado: TDBText;
    LblCodigoMunicipio: TLabel;
    CmbMunicipio: TRxDBLookupCombo;
    BtnBuscarMunicipio: TBitBtn;
    TxtMunicipio: TDBText;
    LblCodigoParroquia: TLabel;
    CmbParroquia: TRxDBLookupCombo;
    BtnBuscarParroquia: TBitBtn;
    TxtParroquia: TDBText;
    DSEstado: TDataSource;
    DSMunicipio: TDataSource;
    DSParroquia: TDataSource;
    LblNumeroCuenta: TLabel;
    TxtNumeroCuenta: TEdit;
    LblCodigoMision: TLabel;
    CmbMision: TRxDBLookupCombo;
    BtnBuscarMision: TBitBtn;
    TxtMision: TDBText;
    DSMision: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure GrdBusquedaCellClick(Column: TColumn);
    procedure BtnBuscarEstadoClick(Sender: TObject);
    procedure BtnBuscarMunicipioClick(Sender: TObject);
    procedure BtnBuscarParroquiaClick(Sender: TObject);
    procedure BtnBuscarMisionClick(Sender: TObject);
  private
    { Private declarations }
    Clase: TCliente;
    Data: TClientDataSet;
    Estado: TEstado;
    DataEstado: TClientDataSet;
    Municipio: TMunicipio;
    DataMunicipio: TClientDataSet;
    Parroquia: TParroquia;
    DataParroquia: TClientDataSet;
    Mision: TMision;
    DataMision: TClientDataSet;
    procedure Agregar;
    procedure Editar;
    procedure Eliminar;
    procedure Guardar;
    procedure Cancelar;
    procedure Refrescar;
    procedure Imprimir;
    procedure Buscar;
    procedure Limpiar;
    procedure Asignar;
    procedure Obtener;
    procedure Mover;
  protected
    function Validar: Boolean; override;
  public
    { Public declarations }
  end;

var
  FrmCliente: TFrmCliente;

implementation

uses
  GenericoBuscador;

{$R *.dfm}

{ TFrmCliente }

procedure TFrmCliente.Agregar;
begin
  //Verificando
  Limpiar;
  TxtCedula.SetFocus
end;

procedure TFrmCliente.Asignar;
begin
  with Clase do
  begin
    TxtCedula.Text := GetCedula;
    CmbPrefijoRif.ItemIndex := CmbPrefijoRif.Items.IndexOf(GetCedula);
    CmbSexo.ItemIndex := CmbSexo.Items.IndexOf(GetSexo);
    DatFechaNacimiento.Date:= GetFechaNacimiento;
    TxtRazonSocial.Text := GetNombres + GetApellidos;
    TxtTelefonos.Text := GetTelefonos;
    TxtEmail.Text := GetEmail;
    DatFechaIngreso.Date:= GetFechaIngreso;
    CmbEstado.KeyValue :=GetCodigoEstado;
    CmbMunicipio.KeyValue := GetCodigoMunicipio;
    CmbParroquia.KeyValue:= GetCodigoParroquia;
    CmbMision.KeyValue:= GetCodigoMision;
    TxtDireccionFiscal1.Text := GetDireccion;
    DataEstado.Locate('codigo',CmbEstado.KeyValue,[loCaseInsensitive]);
    DataMunicipio.Locate('codigo',CmbMunicipio.KeyValue,[loCaseInsensitive]);
    DataParroquia.Locate('codigo',CmbParroquia.KeyValue,[loCaseInsensitive]);
    DataMision.Locate('codigo',CmbMision.KeyValue,[loCaseInsensitive]);
    TxtNumeroCuenta.Text := GetNumeroCuenta;
  end;
end;

procedure TFrmCliente.BtnBuscarEstadoClick(Sender: TObject);
begin
  Buscador(Self,Estado,'codigo','Busqueda de Estado',nil,DataEstado);
  CmbEstado.KeyValue := DataEstado.FieldValues['codigo'];
end;

procedure TFrmCliente.BtnBuscarMisionClick(Sender: TObject);
begin
  Buscador(Self,Mision,'codigo','Busqueda de Mision',nil,DataMision);
  CmbMision.KeyValue := DataMision.FieldValues['codigo'];
end;

procedure TFrmCliente.BtnBuscarMunicipioClick(Sender: TObject);
begin
  Buscador(Self,Municipio,'codigo','Busqueda de Municipio',nil,DataMunicipio);
  CmbMunicipio.KeyValue := DataMunicipio.FieldValues['codigo'];
end;

procedure TFrmCliente.BtnBuscarParroquiaClick(Sender: TObject);
begin
  Buscador(Self,Parroquia,'codigo','Busqueda de Parroquia',nil,DataParroquia);
  CmbParroquia.KeyValue := DataParroquia.FieldValues['codigo'];
end;

procedure TFrmCliente.Buscar;
begin
  Screen.Cursor := crSQLWait;
  if (Length(Trim(TxtCriterio.Text)) > 0) then
  begin
    Data := Clase.ObtenerListaCriterio(Trim(TxtCriterio.Text));
  end
  else
  begin
    Data := Clase.ObtenerLista;
  end;
  Data.First;
  DS.DataSet := Data;
  GrdBusqueda.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TFrmCliente.Cancelar;
begin
  TxtCedula.Enabled := True;
end;

procedure TFrmCliente.Editar;
begin
  //Se Busca el Elemento
  Clase.SetCedula(Data.FieldValues['cedula']);
  Clase.BuscarCodigo;
  Asignar;
  TxtCedula.Enabled := False;
  CmbPrefijoRif.SetFocus;
end;

procedure TFrmCliente.Eliminar;
begin
  //Se Busca el Elemento
  Clase.SetCedula(Data.FieldValues['cedula']);
  Clase.Buscar;
  Clase.Eliminar;
  MessageBox(Self.Handle, 'El Cliente Ha Sido Eliminado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
  Refrescar;
  Limpiar;
end;

procedure TFrmCliente.FormCreate(Sender: TObject);
begin
  inherited;
  ProcAgregar := Agregar;
  ProcEditar := Editar;
  ProcEliminar := Eliminar;
  ProcGuardar := Guardar;
  ProcCancelar := Cancelar;
  ProcRefrescar := Refrescar;
  ProcImprimir := Imprimir;
  ProcBuscar := Buscar;
  Clase := TCliente.Create;
  //Inicializando DataSets Secundarios
  Estado:= TEstado.Create;
  Municipio:= TMunicipio.Create;
  Parroquia:= TParroquia.Create;
  Mision:= TMision.Create;
  DataEstado:= Estado.ObtenerLista;
  DataMunicipio:= Municipio.ObtenerLista;
  DataParroquia:= Parroquia.ObtenerLista;
  DataMision:= Mision.ObtenerLista;
  DataEstado.First;
  DataMunicipio.First;
  DataParroquia.First;
  DataMision.First;
  DSEstado.DataSet:= DataEstado;
  DSMunicipio.DataSet:= DataMunicipio;
  DSParroquia.DataSet:= DataParroquia;
  DSMision.DataSet:= DataMision;
  TxtEstado.DataSource:= DSEstado;
  TxtEstado.DataField:= 'descripcion';
  TxtMunicipio.DataSource:= DSMunicipio;
  TxtMunicipio.DataField:= 'descripcion';
  TxtParroquia.DataSource:= DSParroquia;
  TxtParroquia.DataField:= 'descripcion';
  TxtMision.DataSource:= DSMision;
  TxtMision.DataField:= 'descripcion';
  ConfigurarRxDBLookupCombo(CmbEstado,DSEstado,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbMunicipio,DSMunicipio,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbParroquia,DSParroquia,'codigo','codigo;descripcion');
  ConfigurarRxDBLookupCombo(CmbMision,DSMision,'codigo','codigo;descripcion');
  Buscar;
end;

procedure TFrmCliente.GrdBusquedaCellClick(Column: TColumn);
begin
  Mover;
end;

procedure TFrmCliente.Guardar;
begin
  if (Agregando) then
  begin
    //Agregando...
    //Se Busca el Codigo
    Clase.SetCedula(TxtCedula.Text);
    if (Clase.BuscarCodigo) then
    begin
      MessageBox(Self.Handle, 'La Cedula Ya Existe!!!', PChar(MsgTituloAdvertencia), MB_ICONWARNING or MB_OK or MB_TOPMOST or MB_TASKMODAL);
      TxtCedula.SetFocus;
      Abort;
    end
    else
    begin
      Obtener;
      Clase.Insertar;
      MessageBox(Self.Handle, 'El Cliente Ha Sido Agregado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    end;
  end
  else
  begin
    //Editando...
    Obtener;
    Clase.Modificar;
    MessageBox(Self.Handle, 'El Cliente Ha Sido Editado Con Exito!!!', PChar(MsgTituloInformacion), MB_ICONINFORMATION or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    TxtCedula.Enabled := True;
  end;
  Refrescar;
  Limpiar;
end;

procedure TFrmCliente.Imprimir;
begin

end;

procedure TFrmCliente.Limpiar;
begin
  TxtCedula.Text := '';
  CmbPrefijoRif.Text := '';
  CmbSexo.Text := '';
  DatFechaNacimiento.Date := Now;
  TxtRazonSocial.Text := '';
  TxtTelefonos.Text := '';
  TxtEmail.Text := '';
  DatFechaIngreso.Date := Now;
  CmbEstado.KeyValue := '';
  CmbMunicipio.KeyValue := '';
  CmbParroquia.KeyValue := '';
  CmbMision.KeyValue := '';
  TxtDireccionFiscal1.Text := '';
  TxtNumeroCuenta.Text := '';
end;

procedure TFrmCliente.Mover;
var
  DSTemp: TDataSet;
begin
  if (DS.DataSet.RecordCount > 0) then
  begin
    DSTemp := DS.DataSet;
    with Clase do
    begin
      SetCedula(DSTemp.FieldValues['cedula']);
      SetNacionalidad(DSTemp.FieldValues['nacionalidad']);
      SetNombres(DSTemp.FieldValues['nombre']);
      SetSexo(DSTemp.FieldByName('sexo').AsString[1]);
      SetFechaNacimiento(DSTemp.FieldByName('fecha_nacimiento').AsDateTime);
      SetDireccion(DSTemp.FieldValues['direccion_fiscal_1']);
      SetTelefono(DSTemp.FieldValues['telefonos']);
      SetEmail(DSTemp.FieldValues['email']);
      SetFechaIngreso(DSTemp.FieldByName('fecha_ingreso').AsDateTime);
      SetCodigoEstado(DSTemp.FieldValues['codigo_estado']);
      SetCodigoMunicipio(DSTemp.FieldValues['codigo_municipio']);
      SetCodigoParroquia(DSTemp.FieldValues['codigo_parroquia']);
      SetCodigoMision(DSTemp.FieldValues['codigo_mision']);
      SetNumeroCuenta(DSTemp.FieldValues['numero_cuenta']);
    end;
    Asignar;
  end
  else
  begin
    Limpiar;
  end;
end;


procedure TFrmCliente.Obtener;
begin
  with Clase do
  begin
    SetCedula(TxtCedula.Text);
    SetNacionalidad(CmbPrefijoRif.Text);
    SetSexo(CmbSexo.Text[1]);
    SetFechaNacimiento(DatFechaNacimiento.Date);
    SetNombres(TxtRazonSocial.Text);
    SetTelefono(TxtTelefonos.Text);
    SetEmail(TxtEmail.Text);
    SetFechaIngreso(DatFechaIngreso.Date);
    SetCodigoEstado(CmbEstado.Text);
    SetCodigoMunicipio(CmbMunicipio.Text);
    SetCodigoParroquia(CmbParroquia.Text);
    SetCodigoMision(CmbMision.Text);
    SetDireccion(TxtDireccionFiscal1.Text);
    SetNumeroCuenta(TxtNumeroCuenta.Text);
  end;
end;

procedure TFrmCliente.Refrescar;
begin
  TxtCriterio.Text := '';
  Buscar;
  //Se Actualizan Los demas Componentes
  DataEstado:= Estado.ObtenerLista;
  DataEstado.First;
  DSEstado.DataSet:= DataEstado;
  DataMunicipio:= Municipio.ObtenerLista;
  DataMunicipio.First;
  DSMunicipio.DataSet:= DataMunicipio;
  DataParroquia:= Parroquia.ObtenerLista;
  DataParroquia.First;
  DSParroquia.DataSet:= DataParroquia;
  DataMision:= Mision.ObtenerLista;
  DataMision.First;
  DSMision.DataSet:= DataMision;
end;

function TFrmCliente.Validar: Boolean;
var
  ListaErrores: TStringList;
begin
  ListaErrores := TStringList.Create;
  (* Validando Informacion del Cliente *)
  //Validando Cedula
  if (Length(Trim(TxtCedula.Text)) = 0) then
  begin
    ListaErrores.Add('La Cedula No Puede Estar En Blanco!!!');
  end;
  //Validando Prefijo
  if (Length(Trim(CmbPrefijoRif.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Asignar un Prefijo Ej. "E" o "V"!!!');
  end;
  //Validando Sexo
  if (Length(Trim(CmbSexo.Text)) = 0) then
  begin
    ListaErrores.Add('El Campo Sexo No Puede Estar En Blanco!!!');
  end;
  //Validando Razon Social
  if (Length(Trim(TxtRazonSocial.Text)) = 0) then
  begin
    ListaErrores.Add('El Nombre No Puede Estar En Blanco!!!');
  end;
  //Validando Telefonos
  if (Length(Trim(TxtTelefonos.Text)) = 0) then
  begin
    ListaErrores.Add('Los Tel�fonos No Puede Estar En Blanco!!!');
  end;
  //Validando Codigo Estado
  if (Length(Trim(CmbEstado.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Estado!!!');
  end;
  //Validando Codigo Municipio
  if (Length(Trim(CmbMunicipio.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar el Municipio!!!');
  end;
  //Validando Codigo Parroquia
  if (Length(Trim(CmbParroquia.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar la Parroquia!!!');
  end;
  //Validando Codigo Mision
  if (Length(Trim(CmbMision.Text)) = 0) then
  begin
    ListaErrores.Add('Debe Seleccionar la Mision!!!');
  end;
  //Validando Direccion Fiscal
  if (Length(Trim(TxtDireccionFiscal1.Text)) = 0) then
  begin
    ListaErrores.Add('La Direcci�n No Puede Estar En Blanco!!!');
  end;
  (* Verificando Errores Encontrados *)
  if (ListaErrores.Count > 0) then
  begin
    MessageBox(Self.Handle, PChar(MensajeError(ListaErrores)), PChar(MsgTituloError), MB_ICONERROR or MB_OK or MB_TOPMOST or MB_TASKMODAL);
    Result := False;
  end
  else
  begin
    Result := True;
  end;
end;


end.


